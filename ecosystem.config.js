module.exports = {
  apps: [{
    name: 'Whitelabel',
    script: './server/index.js',
    args: [
      '--color',
    ],
    node_args: [
      '--inspect=0.0.0.0:9229',
    ],
    watch: [
      'server',
      'ecosystem.config.js',
      'package.json',
    ],
    ignore_watch: [
      'node_modules',
      'scripts',
      'docker',
      'keys',
      'nginx',
    ],
  }],
  deploy: {
    production: {
      user: 'ubuntu',
      host: 'ssh.project-name.degiant.com.br',
      ref: 'origin/master',
      key: 'keys/deploy_rsa',
      ssh_options: 'StrictHostKeyChecking=no',
      repo: 'git@gitlab.com:degiant/whitelabel.git',
      path: '/home/ubuntu',
      'pre-setup': 'eval `ssh-agent -s` && ssh-add /home/ubuntu/.ssh/id_rsa',
      'post-deploy': './scripts/post-deploy.sh',
    },
  },
};
