'use strict'

const _ = require('lodash')
const express = require('express')

const router = express.Router()

function parse(name) {
  let [controllerName, methodName] = name.split('@')
  controllerName = controllerName.toLowerCase().replace('controller', '.controller')

  const controller = require(`./controllers/${controllerName}`)
  const method = controller[methodName]

  if (_.isNil(method) || !_.isFunction(method)) {
    return (req, res, next) => res.status(200).json('Not implemented yet')
  }

  return method
}

console.log('Loading routes...')


// Auth
router.post('/auth/register', parse('AuthController@doSignUp'))
router.post('/auth/login', parse('AuthController@doSignIn'))
router.get('/auth/me', parse('AuthController@getCurrentSession'))

// Products
router.get('/products', parse('ProductController@listProducts'))
router.get('/products/find', parse('ProductCOntroller@searchProductByTerm'))
router.post('/products', parse('ProductController@createProduct'))
router.put('/products/:productId', parse('ProductController@updateProduct'))
router.delete('/products/:productId', parse('ProductController@deleteProduct'))

// Categories
router.get('/categories', parse('CategoryController@listCategories'))
router.get('/categories/find', parse('CategoryController@searchCategoryByTerm'))
router.post('/categories', parse('CategoryController@createCategory'))
router.put('/categories/:categoryId', parse('CategoryController@updateCategory'))
router.delete('/categories/:categoryId', parse('CategoryController@deleteCategory'))

module.exports = router
