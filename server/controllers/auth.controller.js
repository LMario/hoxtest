'use strict'

const authService = require('../services/auth.service')

const { ProfileValidator } = require('../validators')

/**
 * Processa a requisição de login de um usuário em particular
 * e quando tem sucesso retorna um Token que pode ser utilizado
 * em todas outras requisições afim de identificar o cliente e poder
 * utilizar a plataforma normalmente.
 */
exports.doSignIn = async (req, res, next) => {
  const profile = await authService.profileByEmail(req.body.email)

  if (!profile) {
    return res.status(404).json('O e-mail não possui cadastro.')
  }

  if (profile.password !== req.body.password) {
    return res.status(401).json('A senha não coincide com o cadastro.')
  }

  const sessionToken = authService.getToken(profile)
  return res.status(200).json(sessionToken)
}

/**
 * Processa a requisição de registro de novos clientes e antes de inserir
 * os dados no banco de dados faz uma validação em todos os campos garantindo
 * assim a sua integridade.
 *
 * Em caso de invalidez em um ou mais campos, o código 400 é retornado. Em caso
 * de sucesso, retornamos o código 201 junto ao Profile criado no payload de resposta.
 */
exports.doSignUp = async (req, res, next) => {
  const {
    value,
    error,
  } = ProfileValidator.validate(req.body)

  if (error) {
    console.log(error)
    return res.status(400).json('Um ou mais campos são inválidos.')
  }

  const profile = await authService.profileByEmail(value.email)

  if (profile) {
    return res.status(400).json('O e-mail já possui cadastro.')
  }

  const createdProfile = await authService.registerProfile(value)
  return res.status(201).json(createdProfile)
}

/**
 * Processa a requisição verificando o header "token" que deve
 * estar presente na requisição para tentar retornar o Profile
 * da pessoa que submeteu o request.
 *
 * Caso o token tenha expirado ou não seja possível recuperar
 * o Profile, será retornado 401 sugestionando que a autenticação
 * seja realizada novamente pelo consumidor.
 */
exports.getCurrentSession = async (req, res, next) => {
  const profile = await authService.parseToken(req.headers.token)

  if (profile) {
    return res.status(200).json(profile)
  }

  return res.status(401).json('Usuário não autenticado.')
}
