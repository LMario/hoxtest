'use strict'

const categoryService = require('../services/category.service')

const { CategoryValidator } = require('../validators')

exports.listCategories = async (req, res, next) => {
  const queryResult = await categoryService.getCategories()
  return res.status(200).json(queryResult)
}

exports.createCategory = async (req, res, next) => {
  const {
    value,
    error,
  } = CategoryValidator.validate(req.body)

  if (error) {
    return res.status(400).json('Um ou mais campos são inválidos.')
  }

  const createdCategory = await categoryService.createCategory(value)
  return res.status(201).json(createdCategory)
}

exports.searchCategoryByTerm = async (req, res, next) => {
  const query = await categoryService.findByTerm(req.query.search)
  console.log(req.query.search)
  return res.json(query)
}

exports.updateCategory = async (req, res, next) => {
  const { categoryId } = req.params 
  const item = req.body
  await categoryService.updateCategoryByID(item, categoryId)
  return res.status(200).json("Categoria atualizada com sucesso!")
}


/**
 * Processa a requisição e remove a Category pelo ID caso essa
 * exista no banco de dados.
 *
 * Sempre retorna código 200, independente de existir ou não o ID.
 */
exports.deleteCategory = async (req, res, next) => {
  const { categoryId } = req.params

  await categoryService.deleteCategory(categoryId)
  return res.status(200).json("Categoria deletada com sucesso!")
}
