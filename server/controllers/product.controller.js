'use strict'

const productService = require('../services/product.service')
const { ProductValidator } = require('../validators')

/**
 * Processa a requisição de login de um usuário em particular
 * e quando tem sucesso retorna um Token que pode ser utilizado
 * em todas outras requisições afim de identificar o cliente e poder
 * utilizar a plataforma normalmente.
 */
exports.listProducts = async (req, res, next) => {
    const queryResult = await productService.getProducts()

    return res.status(200).json(queryResult)
}

exports.searchProductByTerm = async (req, res, next) => {
    const query = await productService.findByTerm(req.query.search)

    return res.json(query)
}


/**
 * Processa a requisição de registro de novas categorias e antes de inserir
 * os dados no banco de dados faz uma validação em todos os campos garantindo
 * assim a sua integridade.
 *
 * Em caso de invalidez em um ou mais campos, o código 400 é retornado. Em caso
 * de sucesso, retornamos o código 201 junto a Product criada no payload de resposta.
 */
exports.createProduct = async (req, res, next) => {
    const {
        value,
        error,
    } = ProductValidator.validate(req.body)

    if (error) {
        return res.status(400).json('Um ou mais campos são inválidos.')
    }

    const createdProduct = await productService.createProduct(value)

    return res.status(201).json(createdProduct)
}

exports.updateProduct = async (req, res, next) => {
    const { productId } = req.params
    const item = req.body

    await productService.updateProductByID(item, productId)

    return res.status(200).json('Produto atualizado com sucesso!')
}

/**
 * Processa a requisição e remove a Product pelo ID caso essa
 * exista no banco de dados.
 *
 * Sempre retorna código 200, independente de existir ou não o ID.
 */
exports.deleteProduct = async (req, res, next) => {
    const { productId } = req.params

    await productService.deleteProduct(productId)

    return res.status(200).json("Produto deletado com sucesso!")
}
