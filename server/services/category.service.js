'use strict'

const { Category } = require('../models')
const { Op } = require('sequelize');

exports.createCategory = values => Category.create(values)

exports.getCategories = () => Category.findAndCountAll({limit: 10})

exports.updateCategoryByID = async (item, id) => Category.update(item, { where: { id } })

exports.deleteCategory = id => Category.destroy({ where: { id } })

exports.findById = id => Category.findByPk(id)

exports.findByTerm = async (term) => {
    term = term.replace(/[^a-zá-ú0-9]+/ig, '%')
    console.log(term)
    const query = await Category.findAndCountAll({
      where: {
        [Op.or]: [
          { id: { [Op.like]: `%${term}%` } },
          { name: { [Op.like]: `%${term}%` } },
        ],
      },
    })
    return query
  }