'use strict'

const jwt = require('jsonwebtoken')

const { tokenSecret } = require('../config')

const { Profile } = require('../models')

exports.profileByEmail = email => Profile.scope('sensitive').findOne({ where: { email } })

exports.registerProfile = values => Profile.create(values)

exports.parseToken = (token) => {
  if (typeof token === 'string') {
    const id = jwt.decode(token, tokenSecret)

    if (id) {
      return Profile.findByPk(id)
    }

    return false
  }

  return false
}

exports.getToken = profile => jwt.sign(profile.id, tokenSecret)
