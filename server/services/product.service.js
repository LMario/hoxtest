'use strict'

const _ = require('lodash');
const { Op } = require('sequelize');
const { sequelize } = require('../models')


const {
  Product,
  Category
} = require('../models')

exports.findById = id => Product.findByPk(id)

exports.createProduct = async values => {
  return await Product.create(values)
}

exports.updateProductByID = async (item, id) => Product.update(item, { where: { id } })

exports.getProducts = async () => {
  const result = await Product.findAndCountAll({
    include: [{ model: Category}],
    order: sequelize.literal('categoryId ASC'),
    limit: 10
  })

  // Return result
  return result
}

exports.findByTerm = async (term) => {
  term = term.replace(/[^a-zá-ú0-9]+/ig, '%')

  const query = await Product.findAndCountAll({
    include: [{ model: Category }],
    where: {
      [Op.or]: [
        { id: { [Op.like]: `%${term}%` } },
        { name: { [Op.like]: `%${term}%` } },
      ],
    },
    limit: 10,
  })
  return query
}

exports.deleteProduct = id => Product.destroy({ where: { id } })
