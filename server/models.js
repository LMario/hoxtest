'use strict'

const Sequelize = require('sequelize')
const sequelize = new Sequelize('mysql://root@database:3306/hoxprod', { logging: false })

class Profile extends Sequelize.Model { }
class Category extends Sequelize.Model { }
class Product extends Sequelize.Model { }


const defaultScope = { attributes: { exclude: ['createdAt', 'updatedAt'] } }

Profile.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  phoneNumber: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  password: {
    type: Sequelize.TEXT,
    allowNull: false,
  },
  createdAt: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.NOW,
  },
  updatedAt: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.NOW,
  },
}, {
  defaultScope: {
    attributes: { exclude: ['password'] }
  },
  sequelize,
  modelName: 'profile',
  tableName: 'profile',
})

Profile.addScope('sensitive', {
  attributes: { include: ['password'] }
})

Product.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  categoryId: {
    type: Sequelize.INTEGER,
    references: {
      model: 'profile',
      key: 'id',
    },
    allowNull: false,
    onUpdate: 'cascade',
    onDelete: 'cascade',
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  manufacturingDate: {
    type: Sequelize.DATE,
    allowNull: false,
  },
  perishableProduct: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
  },
  expirationDate: {
    type: Sequelize.DATE,
    allowNull: true,
  },
  price: {
    type: Sequelize.DECIMAL(11, 2),
    allowNull: false,
  },
  createdAt: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.NOW,
  },
  updatedAt: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.NOW,
  },
}, {

  sequelize,
  modelName: 'products',
  tableName: 'products',

})

Category.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  createdAt: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.NOW,
  },
  updatedAt: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.NOW,
  },
}, {
  sequelize,
  modelName: 'category',
  tableName: 'category',
  defaultScope,
})


Product.belongsTo(Category, { foreignKey: 'categoryId' })

module.exports = {
  Profile,
  Category,
  Product,
  Sequelize,
  sequelize,
}
