'use strict'

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'

const config = require('../config.json')
const sessionLoader = require('./session')
const path = require('path')
const express = require('express')
const session = require('express-session')
const MySQLStore = require('express-mysql-session')(session)
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const helmet = require('helmet')
const cors = require('cors')

const { exec } = require('child_process');

console.log('Running migrations... %s', path.join(__dirname, '../../'))

exec('npm run migrate', {
  env: process.env,
  cwd: path.join(__dirname, '../'),
}, (err, stdout, stderr) => {
  if (err) {
    console.info('Migrations failed to run:')
    console.error(err)
  } else {
    console.info('Migrations runned successfully')
  }
})

const server = express()

const sessionStore = new MySQLStore({
  host: 'database',
  user: 'root',
  database: 'hoxprod',
  password: null,
  port: 3306,
})

server.use(cors())
server.use(helmet())
server.use(bodyParser.json())
server.use(bodyParser.urlencoded({ extended: false }))
server.use(cookieParser())

server.use(session({
  key: 'hoxprod',
  secret: '99SA8!KK23L!#9@',
  resave: true,
  overwrite: true,
  signed: true,
  rolling: true,
  saveUninitialized: true,
  store: sessionStore,
  cookie: {
    path: '/',
    expires: true,
    maxAge: (30 * 60 * 1000),
    domain: config.cookie.domain,
    httpOnly: config.cookie.httpOnly,
    secure: config.cookie.secure,
  },
}))

server.set('trust proxy', 1)
server.use(sessionLoader)
server.use('/painel', express.static(path.resolve(path.join(__dirname, '../public'))))
server.use('/static', express.static(path.resolve(path.join(__dirname, '../public/static'))))
server.use('/', require('./router'))

module.exports = server.listen(3000)
