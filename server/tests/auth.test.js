const shortid = require('shortid');
const request = require('supertest');
const { expect } = require('chai')
const app = 'http://app:3000'


/*Run tests on Auth controllers */
describe('#Auth', function () {
    describe('Login API', function () {
        describe('POST /auth/login must have', function () {
            this.contentType = ""
            this.token = ""
            it('statusCode 200', function (done) {
                request(app)
                    .post('/auth/login')
                    .set('Accept', 'application/json')
                    .send({
                        email:"admin@hox.com",
                        password: "P4SSW0RD!"
                    })
                    .end(function (err, res) {
                        contentType = res.type
                        token = res.text
                        expect(err).to.be.null
                        expect(res.statusCode).to.equal(200)
                        done();
                    });
            });
            it('Content-Type application/json', function (done) {
                expect(contentType).to.equal('application/json')
                done();
            });
            it('Token "eyJhbGciOiJIUzI1NiJ9.NA.IODvWS3aArdQJPtBrt3k51woa7R-6TNDpI2ShGU_RNc"', function (done) {
                expect(token).to.equal('"eyJhbGciOiJIUzI1NiJ9.NA.IODvWS3aArdQJPtBrt3k51woa7R-6TNDpI2ShGU_RNc"')
                done();
            });
        });

      describe('Create API', function () {
            this.contentType = ""
            it('statusCode 201', function (done) {
                request(app)
                    .post('/auth/register')
                    .set('Accept', 'application/json')
                    .send({
                        name: "Mário",
                        email: `admin${shortid()}@hox.com`,
                        password: "P4SSW0RD!",
                        phoneNumber: 99999999
                    })
                    .end(function (err, res) {
                        contentType = res.type
                        token = res.text
                        expect(err).to.be.null
                        expect(res.statusCode).to.be.within(200, 201)
                        done();
                    });
            });

            it('Content-Type application/json', function (done) {
                expect(contentType).to.equal('application/json')
                done();
            });
        }); 
    });

});
