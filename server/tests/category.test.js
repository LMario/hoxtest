const shortid = require('shortid');
const request = require('supertest');
const { expect } = require('chai')
const app = 'http://app:3000'

/* Run tests on Categories controllers */
describe('#Categories', function () {
    describe('List API', function () {
        this.contentType = ""
        it('Must have statusCode 200', function (done) {
            request(app)
                .get('/categories')
                .set('Accept', 'application/json')
                .end(function (err, res) {
                    contentType = res.type
                    expect(err).to.be.null
                    expect(res.statusCode).to.equal(200)
                    done();
                });
        });
        it('Must have Content-Type application/json', function (done) {
            expect(contentType).to.equal('application/json')
            done();
        });
    });

    describe('Create API', function () {
        this.contentType = ""
        it('Must have statusCode 201', function (done) {
            request(app)
                .post('/categories')
                .set('Accept', 'application/json')
                .send({ name: 'Carros'})
                .end(function (err, res) {
                    contentType = res.type
                    expect(res.statusCode).to.be.within(200, 201)
                    expect(err).to.be.null
                    done();
                });
        });

        it('Must have Content-Type application/json', function (done) {
            expect(contentType).to.equal('application/json')
            done();
        });

    });
    describe('Update API', function () {
        this.contentType = ""
        it('Must have statusCode 200', function (done) {
            request(app)
                .put('/categories/1')
                .set('Accept', 'application/json')
                .send({ name: 'Motos'})
                .end(function (err, res) {
                    expect(err).to.be.null
                    expect(res.statusCode).to.equal(200)
                    done();
                });
        });

        it('Must have Content-Type application/json', function (done) {
            expect(contentType).to.equal('application/json')
            done();
        });

    });

    describe('Delete API', function () {
        this.contentType = ""
        it('Must have statusCode 200', function (done) {
            request(app)
                .delete('/categories/1')
                .set('Accept', 'application/json')
                .end(function (err, res) {
                    contentType = res.type
                    expect(res.statusCode).to.equal(200)
                    expect(err).to.be.null
                    done();
                });
        });

        it('Must have Content-Type application/json', function (done) {
            expect(contentType).to.equal('application/json')
            done();
        });

    });
});