const shortid = require('shortid');
const request = require('supertest');
const { expect } = require('chai')
const app = 'http://app:3000'

/* Run tests on Categories controllers */
describe('#Products', function () {
    describe('List API', function () {
        this.contentType = ""
        it('Must have statusCode 200', function (done) {
            request(app)
                .get('/products')
                .set('Accept', 'application/json')
                .end(function (err, res) {
                    contentType = res.type
                    expect(err).to.be.null
                    expect(res.statusCode).to.equal(200)
                    done();
                });
        });
        it('Must have Content-Type application/json', function (done) {
            expect(contentType).to.equal('application/json')
            done();
        });
    });

    describe('Create API', function () {
        this.contentType = ""
        it('Must have statusCode 201', function (done) {
            request(app)
                .post('/products')
                .set('Accept', 'application/json')
                .send({
                    id: 1,
                    categoryId: 2,
                    name: "Pneu",
                    manufacturingDate: "2018-08-28T11:46:30.120",
                    perishableProduct: true,
                    expirationDate: "2018-08-29T11:46:30.1207",
                    price: 120
                })
                .end(function (err, res) {
                    contentType = res.type
                    expect(res.statusCode).to.be.within(200, 201)
                    expect(err).to.be.null
                    done();
                });
        });

        it('Must have Content-Type application/json', function (done) {
            expect(contentType).to.equal('application/json')
            done();
        });

    });
    describe('Update API', function () {
        this.contentType = ""
        it('Must have statusCode 200', function (done) {
            request(app)
                .put('/products/1')
                .set('Accept', 'application/json')
                .send({
                    id: 1,
                    categoryId: 2,
                    name: "Pneu",
                    manufacturingDate: "2018-08-28T11:46:30.120",
                    perishableProduct: true,
                    expirationDate: "2018-08-29T11:46:30.1207",
                    price: 120
                })
                .end(function (err, res) {
                    expect(err).to.be.null
                    expect(res.statusCode).to.equal(200)
                    done();
                });
        });

        it('Must have Content-Type application/json', function (done) {
            expect(contentType).to.equal('application/json')
            done();
        });

    });

    describe('Delete API', function () {
        this.contentType = ""
        it('Must have statusCode 200', function (done) {
            request(app)
                .delete('/products/1')
                .set('Accept', 'application/json')
                .end(function (err, res) {
                    contentType = res.type
                    expect(res.statusCode).to.equal(200)
                    expect(err).to.be.null
                    done();
                });
        });

        it('Must have Content-Type application/json', function (done) {
            expect(contentType).to.equal('application/json')
            done();
        });

    });
});