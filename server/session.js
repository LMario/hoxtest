'use strict'

const authService = require('./services/auth.service')

module.exports = async function loadProfile(req, res, next) {
    const {
        token,
    } = req.headers

    if (token) {
        req.profile = await authService.parseToken(token)
    }

    return next()
}
