'use strict';

const Joi = require('joi')

exports.ProfileValidator = Joi.object({
  name: Joi.string().min(3).max(50).required(),
  email: Joi.string().email().required(),
  phoneNumber: Joi.number().integer().required(),
  password: Joi.string().min(1).max(255).required(),
}).options({ stripUnknown: true })

exports.CategoryValidator = Joi.object({
  name: Joi.string().min(3).max(50).required(),
}).options({ stripUnknown: true })


exports.ProductValidator = Joi.object({
  categoryId: Joi.number().positive().required(),
  name: Joi.string().min(1).max(60).required(),
  perishableProduct: Joi.boolean().required(),
  manufacturingDate: Joi.date().iso().required(),
  expirationDate : Joi.date().iso().greater(Joi.ref('manufacturingDate')).required(),
  price: Joi.number().required(),
}).options({ stripUnknown: true })
