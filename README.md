# HoxTest


## Documentaçaõ / Documentation
[Clique aqui / Click here](https://speeding-shuttle-124886.postman.co/collections/11010049-141abcfc-bd9c-4ee7-a1f2-7ec34c85626a?version=latest&workspace=cc5cbd11-5c86-49bb-826a-36b5801c177a)

## Funcionalidades e rotas / Features and Routes

### Autenticação / Authentication
   - Criar perfis e registrar no banco de dados / Create profiles and register in the database
   - Entrar em um perfil e gerar token de acesso / Log in an account and receive a access token
   - Obter informações de um perfil através do token / Get profile info using the access token

### Categorias / Categories
   - Criar categorias / Create Categories
   - Editar categorias / Update Categories 
   - Pesquisar categorias por termo / Search categories by term 
   - Deletar categorias / Delete categories

### Produtos / Products 
   - Criar produtos / Create Products
   - Editar produtos / Update Products 
   - Pesquisar produtos por termo / Search products by term 
   - Deletar produtos / Delete Products

## Sobre/About

### Autor/Author: *Mário Soares*
### Data/Date: *04/11/2020*