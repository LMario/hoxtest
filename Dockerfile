FROM keymetrics/pm2:10-alpine

WORKDIR /app
COPY ["./yarn.lock", "./package.json", "./"]

RUN npm install babel-cli rimraf mocha -g
RUN yarn install

COPY server/  migrations/ scripts/ package.json yarn.lock  ecosystem.config.js ./

EXPOSE 3000 9229
CMD pm2 start --no-daemon ecosystem.config.js
